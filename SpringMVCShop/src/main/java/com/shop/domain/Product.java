package com.shop.domain;


public class Product {
    private String name;
    private String category;
    private String id;

    public Product(String name, String category, String id) {
        this.name = name;
        this.category = category;
        this.id = id;

    }

//    @Override
//    public String toString() {
//        return "Product{" +
//                "name='" + name + '\'' +
//                ", category='" + category + '\'' +
//                ", id='" + id + '\'' +
//                ", cart='" + cart + '\'' +
//                '}';
//    }
//    @Override
//    public String toString() {
//        return "Product{" +
//                "name='" + name + '\'' +
//                ", category='" + category + '\'' +
//                ", id='" + id + '\'' +
//                ", cart='" + cart + '\'' +
//                '}';
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
