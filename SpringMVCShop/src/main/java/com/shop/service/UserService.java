package com.shop.service;


import com.shop.domain.User;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    public User findUserByLogin(String login) {
        //return enrolleeRepository.findEnrolleeByLogin(login);
        return new User("user", "user");
    }


    public User authenticate(User user) {
        User foundUser = this.findUserByLogin(user.getLogin());
//        if (foundUser == null) {
//            throw new RuntimeException();
//        }
        if (!foundUser.getPass().equals(user.getPass())) {
            throw new RuntimeException();
        }
        return foundUser;
    }

    public User login(User user){
        User foundUser = this.findUserByLogin(user.getLogin());
//        if (!(foundUser == null)) {
//            throw new RuntimeException();
//        }
        return user;
    }
}
