package com.shop.controller;


import com.shop.domain.Product;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/main/cart/")
public class CartController {
    List<Product> products = Collections.synchronizedList(new ArrayList<>());
    //List<Product> products = new ArrayList<>();
    Product milk = new Product("milk", "A", "0");
    Product bread = new Product("bread", "B", "1");
    Product cheese = new Product("cheese", "B", "2");

    @GetMapping
    public List<Product> cart() {
        return products;
    }

    @PostMapping("add/{id}")
    public String addcart(@PathVariable String id) {
        if (id.equals("0")) {
            products.add(milk);
        }
        if (id.equals("1")) {
            products.add(bread);
        }
        if (id.equals("2")) {
            products.add(cheese);
        }
        String result = new String("goods");
        return result;
    }

    @DeleteMapping("{id}")
    public List<Product> deletcart(@PathVariable String id) {
        for (Product product : products) {
            if (product.getId().equals(id)) {
                products.remove(product);
                break;
            }
        }
        return products;
    }

}
