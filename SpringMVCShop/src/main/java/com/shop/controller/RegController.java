package com.shop.controller;


import com.shop.domain.User;
import com.shop.manager.UserManager;
import com.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class RegController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserManager userManager;

    @GetMapping("/reg")
    public String user() {
        return "reg";
    }

    @PostMapping("/reg")
    public String regPost(@Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(System.out::println);
            return "reg";
        }
        User foundUser = userService.login(user);
        userManager.setUser(foundUser);
        //userService.save(foundUser);
        return "redirect:/main";
    }
}
