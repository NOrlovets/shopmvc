package com.shop.controller;

import com.shop.domain.Product;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@RestController
@RequestMapping("/main/search/")
public class SearchController {
    List<Product> products = Collections.synchronizedList(new ArrayList<>());
    //List<Product> products = new ArrayList<>();
    Product milk = new Product("milk", "A", "0");
    Product bread = new Product("bread", "B", "1");
    Product cheese = new Product("cheese", "B", "2");
    Product coffee = new Product("coffee", "A", "3");

    List<Product> findproducts = Collections.synchronizedList(new ArrayList<>());
    //List<Product> findproducts = new ArrayList<>();

    @GetMapping("{id}")
    public List<Product> search(@PathVariable String id) {
        products.clear();
        products.add(milk);
        products.add(bread);
        products.add(cheese);
        products.add(coffee);

        findproducts.clear();

        for (Product product : products) {
            if (product.getCategory().equals(id)) {
                findproducts.add(product);
            }
        }
        return findproducts;
    }
}
