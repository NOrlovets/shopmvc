//search pop-up
$(document).ready(function () {
    $(".btn-info").click(function () {
        $(".search").fadeIn(300);
    });

    $(".search > span").click(function () {
        $(".search").fadeOut(300);
    });
});

var goods = 0;

//product pop-up
$(document).ready(function () {
    $(".button").click(function () {
        $(".prodid").empty();
        $(".product").fadeIn(300);
        var rand = Math.floor(Math.random() * 3);
        $(".prodid").append(rand);
        var elemY = (this).offsetTop;
        $(".product").css('top', elemY);
        $(".description").fadeIn(300);
    });
    $(".product > span").click(function () {
        $(".product").fadeOut(300);
    });
});

//matching pass in reg
$('#password, #checkpassword').on('keyup', function () {
    if ($('#password').val() == $('#checkpassword').val()) {
        $('#message').html('Matching').css('color', 'green');
    } else
        $('#message').html('Not Matching').css('color', 'red');
});

//get cart
$(document).ready(function () {
    $(".btn-primary").click(function () {
        $(".shop").fadeIn(300);
        $("#blur").addClass('blur-in');
        $('#ul-id').empty();
        $.ajax({
            type: "GET",
            url: "/main/cart/",
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    //console.log(item.name);
                    $('#ul-id').append("<li>" +
                        "<p>" + "<span class='id'>" + item.id + "</span>" +
                        " " + item.name +
                        "<span class='delete'>✖</span>" +
                        "</p>" + "</li>");
                }
                setClickListener();
            }
        });

    });

    //delete from cart
    function setClickListener() {
        $(".delete").click(function () {
            //console.log($(this).parent().html());
            var hh = $(this).parent().find(".id").html();
            $.ajax({
                type: "DELETE",
                url: "/main/cart/" + hh,
                //data:{ id: hh},
                success: function (data) {
                    goods--;
                    $('#ul-id').empty();
                    $('#cart').html(goods);
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        //console.log(item.name);
                        $('#ul-id').append("<li>" +
                            "<p>" + "<span class='id'>" + item.id + "</span>" +
                            " " + item.name +
                            "<span class='delete'>✖</span>" +
                            "</p>" + "</li>");
                    }
                    setClickListener();
                }
            });
        });
    }

    $(".shop > span").click(function () {
        $("#blur").removeClass('blur-in');
        $(".shop").fadeOut(300);
    });
});

//add to cart
$(document).ready(function () {
    $("#butcen").click(function () {
        goods++;
        //console.log($(".prodid").text());
        var gg = $(".prodid").text();
        $.ajax({
            type: "POST",
            url: "/main/cart/add/" + gg,
            //data:{ id: gg},
            success: function (data) {
                $('#ul-id').empty();
                $('#cart').html(goods);
            }
        });
    });
});

//search
$(document).ready(function () {
    $("#sbutton").click(function () {
        //console.log($("#searchinp").val());
        $('#sul-id').empty();
        $.ajax({
            type: "GET",
            url: "/main/search/" + $("#searchinp").val(),
            //success: function (data) {
            //    //console.log("log2");
            //    $('#result').html(data);
            //}
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    //console.log(item.name);
                    $('#sul-id').append("<li>" +
                        "<p>" + "<span class='id'>" + item.id + "</span>" +
                        " " + item.name +
                        "</p>" + "</li>");
                }
            }
        });
    });
});
